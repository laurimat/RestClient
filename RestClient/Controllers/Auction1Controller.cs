﻿using RestClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace RestClient.Controllers
{
    public class Auction1Controller : Controller
    {
        // GET: Auction
        public ActionResult Index()
        {
            List<Auction> auctions = new List<Auction>();
       
            HttpClient client = new HttpClient();
            var url = "http://uptime-auction-api.azurewebsites.net/api/Auction";
            var response = client.GetAsync(url).Result;
            if(response.IsSuccessStatusCode)
            {
                auctions = response.Content.ReadAsAsync<List<Auction>>().Result;

            }
            ViewBag.Category = new SelectList(auctions.Select( x => x.ProductCategory).Distinct().ToList(), String.Empty);
            return View(auctions.Where(x => x.BiddingEndDate > DateTime.Now.Subtract(TimeSpan.FromDays(7))));
        }
    }
}