﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestClient.Models
{
    public class Auction
    {
         public int Id { get; set; }
         public string ProductId { get; set; }
         public string ProductName { get; set; }
         public string ProductDescription { get; set; }
         public string ProductCategory { get; set; }
         public DateTime BiddingEndDate { get; set; }
    }

    public class User
    {
        public string UserId { get; set; }
        public string Name { get; set; }
    }

    public class Offer
    {
        public int Id { get; set; }
        public double Amount { get; set; }
        public string UserId { get; set; }
        public int AuctionId { get; set; }
    }
}